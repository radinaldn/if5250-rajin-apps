package org.informatika.rajin.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.Timestamp
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.ktx.toObject
import org.informatika.rajin.databinding.PresenceListItemBinding
import org.informatika.rajin.model.Presence
import java.text.SimpleDateFormat
import java.time.Duration
import java.util.*
import java.time.temporal.ChronoUnit

private val Timestamp?.formatted: String
    get() {
        val sfd = SimpleDateFormat("dd-MM-yyyy HH:mm:ss")
        return sfd.format(this?.toDate()?.time?.let { Date(it) })
    }

private val Timestamp?.formattedDateOnly: String
    get() {
        val sfd = SimpleDateFormat("EEEE, dd MMMM yyyy")
        return sfd.format(this?.toDate()?.time?.let { Date(it) })
    }

private val Timestamp?.formattedTimeOnly: String
    get() {
        val sfd = SimpleDateFormat("HH:mm:ss")
        return sfd.format(this?.toDate()?.time?.let { Date(it) })
    }

open class PresenceAdapter(query: Query, private val listener: OnPresenceSelectedListener) : FirestoreAdapter<PresenceAdapter.ViewHolder>(query) {

    interface OnPresenceSelectedListener {
        fun onPresenceSelected(presence: Presence)
    }

    class ViewHolder(val binding: PresenceListItemBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(
            snapshot: DocumentSnapshot,
            listener: OnPresenceSelectedListener?,
        ){
            val presence = snapshot.toObject<Presence>()

            binding.tvJenis.text = presence?.jenis
            binding.tvTimeCreate.text = presence?.checkIn?.waktu?.formattedDateOnly
            binding.tvCheckIn.text = "Masuk : ${presence?.checkIn?.waktu?.formattedTimeOnly}"
            binding.tvCheckOut.text = "Pulang : ${if (presence?.checkOut==null) "-" else presence?.checkOut?.waktu?.formattedTimeOnly}"

            presence?.checkOut?.let {
                val startDate: Date = presence?.checkIn?.waktu!!.toDate()
                val endDate: Date = it?.waktu!!.toDate()

                val durationInMillis: Long = endDate.time - startDate.time
                val hours: Long = durationInMillis / (1000 * 60 * 60)
                val minutes: Long = durationInMillis / (1000 * 60) % 60
                val seconds: Long = durationInMillis / 1000 % 60

                binding.tvDurasi.text = "Durasi Kerja : $hours Jam, $minutes Menit, $seconds Detik"
            }
            // TODO: lanjutkan binding
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        Log.d("PresenceAdapter", "onCreateViewHolder invoked")
        return ViewHolder(PresenceListItemBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        ))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Log.d("PresenceAdapter", "onBindViewHolder item ke $position")
        holder.bind(getSnapshot(position), listener)
    }
}