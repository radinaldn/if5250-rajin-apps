package org.informatika.rajin.ui.presence

import org.informatika.rajin.model.Filters

interface FilterListener {
    fun onFilter(filters: Filters)
}