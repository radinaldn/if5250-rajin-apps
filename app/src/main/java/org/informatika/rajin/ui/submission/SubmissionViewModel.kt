package org.informatika.rajin.ui.submission

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class SubmissionViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is submission Fragment"
    }
    val text: LiveData<String> = _text
}