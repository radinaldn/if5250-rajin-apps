package org.informatika.rajin.ui.home

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import org.informatika.rajin.model.Staff
import org.informatika.rajin.model.UnitKerja
import org.informatika.rajin.util.FirebaseStaffService

private const val TAG = "HomeViewModel"
class HomeViewModel : ViewModel() {

//    private val _staff = MutableLiveData<Staff>()
//    val staff: LiveData<Staff> = _staff

    private val _staff = MutableLiveData<Staff?>(null)
    val staff: LiveData<Staff?> = _staff

    private val _unitKerja = MutableLiveData<UnitKerja>(null)
    val unitkerja: LiveData<UnitKerja?> = _unitKerja

    init {
        viewModelScope.launch {
            FirebaseStaffService.getUser().collect{ staff ->

                _staff.value = staff

                _unitKerja.value = FirebaseStaffService.getUserUnitKerja(staff!!.unitKerja!!)
            }
        }
    }

//    private val _text = MutableLiveData<String>().apply {
//        value = "This is home Fragment"
//    }
//    val text: LiveData<String> = _text
}