package org.informatika.rajin.ui.home

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.*
import androidx.navigation.NavController
import androidx.navigation.Navigation
import kotlinx.coroutines.launch
import org.informatika.rajin.R
import org.informatika.rajin.databinding.FragmentHomeBinding
import org.informatika.rajin.ui.TakeAttendanceActivity

class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private lateinit var navController: NavController

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        val viewModel =
            ViewModelProvider(this)[HomeViewModel::class.java]

        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root

        viewModel.staff.observe(viewLifecycleOwner, Observer {
            it.let {
                binding.tvNama.text = it?.nama
                binding.tvNoInduk.text = it?.noInduk

            }
        })

        viewModel.unitkerja.observe(viewLifecycleOwner, Observer{
            it.let {
                binding.tvOrganisasi.text = it?.nama
            }
        })



        val btIsiKehadiran: Button = binding.btIsiKehadiran
        btIsiKehadiran.setOnClickListener {
            val intent = Intent(activity, TakeAttendanceActivity::class.java)
            startActivity(intent)
        }
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.navController = Navigation.findNavController(view)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}