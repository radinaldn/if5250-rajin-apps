package org.informatika.rajin.ui.submission

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import org.informatika.rajin.databinding.FragmentSubmissionBinding

class SubmissionFragment : Fragment() {

    private var _binding: FragmentSubmissionBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        val submissionViewModel =
            ViewModelProvider(this).get(SubmissionViewModel::class.java)

        _binding = FragmentSubmissionBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val textView: TextView = binding.textSubmission
        submissionViewModel.text.observe(viewLifecycleOwner) {
            textView.text = it
        }
        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}