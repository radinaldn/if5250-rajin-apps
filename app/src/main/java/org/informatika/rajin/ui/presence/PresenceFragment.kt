package org.informatika.rajin.ui.presence

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreException
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import org.informatika.rajin.adapter.PresenceAdapter
import org.informatika.rajin.databinding.FragmentPresenceBinding
import org.informatika.rajin.model.Filters
import org.informatika.rajin.model.Presence

class PresenceFragment : Fragment(), FilterListener, PresenceAdapter.OnPresenceSelectedListener {

    lateinit var firestore: FirebaseFirestore
    private var query: Query? = null
    private var adapter: PresenceAdapter? = null

    private var _binding: FragmentPresenceBinding? = null

    private lateinit var viewModel:PresenceViewModel


    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {

        _binding = FragmentPresenceBinding.inflate(inflater, container, false)
        val root: View = binding.root

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // View model
        viewModel =
            ViewModelProvider(this).get(PresenceViewModel::class.java)

        // Enable Firestore logging
        FirebaseFirestore.setLoggingEnabled(true)

        // Firestore
        firestore = Firebase.firestore

        // Get the 10 newest presence
        query = firestore.collection(Presence.COLLECTION_NAME)
            .orderBy(Presence.FIELD_TIME_CREATE)
            .limit(10)

        query!!.get().addOnCompleteListener { task ->
            if (task.isSuccessful){
                val result = task.result
                result?.let {
                    result.documents.mapNotNull { snapshot ->
                        Log.d("PresenceFragment", "snapshot = ${snapshot["staff_id"]}")
                        val p = snapshot.toObject(Presence::class.java)
                        Log.d("PresenceFragment", p.toString())
                    }
                }
            } else {
                task.exception?.let { Log.e("PresenceFragment", it.stackTraceToString()) }
            }
        }


        // Recyclerview
        query?.let {
            Log.d("PresenceFragment", "here")

            adapter = PresenceAdapter(it, this@PresenceFragment)

//            adapter = object : PresenceAdapter(it, this@PresenceFragment){
//                override fun onDataChanged() {
//                    Log.d("PresenceFragment", "itemCount = $itemCount")
//                    // Show/hide content if the query returns empty
//                    if (itemCount == 0){
//                        binding.recyclerView.visibility = View.GONE
//                        binding.tvTidakAdaData.visibility = View.VISIBLE
//                    } else {
//                        binding.recyclerView.visibility = View.VISIBLE
//                        binding.tvTidakAdaData.visibility = View.GONE
//                    }
//                }
//
//                override fun onError(e: FirebaseFirestoreException) {
//                    // Show a snackbar on errors
//                    Snackbar.make(binding.root, "Error: check logs for info.", Snackbar.LENGTH_LONG).show()
//                }
//            }
            Log.d("PresenceFragment", "here 97")
            binding.recyclerView.adapter = adapter
        }

        binding.recyclerView.layoutManager = LinearLayoutManager(context)
    }

    override fun onStart() {
        super.onStart()

        onFilter(viewModel.filters)

        // Start listening for Firestore updates
        adapter?.startListening()
    }

    override fun onStop() {
        super.onStop()
        adapter?.stopListening()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onFilter(filters: Filters) {
        viewModel.filters = filters
    }

    override fun onPresenceSelected(presence: Presence) {
        Snackbar.make(binding.root, "Presence selected", Snackbar.LENGTH_LONG).show()
    }

}