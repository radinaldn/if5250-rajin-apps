package org.informatika.rajin.util

import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.EventListener
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreException
import kotlinx.coroutines.NonCancellable.cancel
import kotlinx.coroutines.cancel
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.tasks.await
import org.informatika.rajin.model.Staff
import org.informatika.rajin.model.UnitKerja

object FirebaseStaffService {
    private const val TAG = "FirebaseStaffService"
    suspend fun getStaffLoggedIn(): Staff?{
        val db = FirebaseFirestore.getInstance()
        return try {
            var mAuth = FirebaseAuth.getInstance()

            if (mAuth.currentUser==null) throw Exception("Anda belum login")

            db.collection(Staff.COLLECTION_NAME).document(mAuth.currentUser!!.uid).get().await().toObject(Staff::class.java)
        } catch (e: Exception){
            Log.e(TAG, "Error getting staff", e)
            null
        }
    }

    fun getUser(): Flow<Staff?> = callbackFlow {
        val firestore = FirebaseFirestore.getInstance()

        val listener = object : EventListener<DocumentSnapshot> {
            override fun onEvent(snapshot: DocumentSnapshot?, exception: FirebaseFirestoreException?) {
                if (exception != null) {
                    // An error occurred
                    cancel()
                    return
                }

                if (snapshot != null && snapshot.exists()) {
                    // The user document has data
                    val user = snapshot.toObject(Staff::class.java)
                    trySend(user)
                } else {
                    // The user document does not exist or has no data
                }
            }
        }


        var mAuth = FirebaseAuth.getInstance()

        if (mAuth.currentUser==null) throw Exception("Anda belum login")
        val registration = firestore.collection(Staff.COLLECTION_NAME).document(mAuth.currentUser!!.uid).addSnapshotListener(listener)
        awaitClose { registration.remove() }
    }

    suspend fun getUserUnitKerja(reference: DocumentReference): UnitKerja? {
        val db = FirebaseFirestore.getInstance()
        return try {
            db.document(reference.path).get().await().toObject(UnitKerja::class.java)
        } catch (e: Exception){
            Log.e(TAG, "Error getting unit_kerja", e)
            null
        }
    }
}