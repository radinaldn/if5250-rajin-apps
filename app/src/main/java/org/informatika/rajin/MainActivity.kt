package org.informatika.rajin

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import org.informatika.rajin.databinding.ActivityMainBinding
import org.informatika.rajin.model.Presence
import org.informatika.rajin.model.Staff


private const val TAG = "MainActivity"

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var mAuth: FirebaseAuth
    lateinit var firestore: FirebaseFirestore
    private var query: Query? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mAuth = FirebaseAuth.getInstance()
        Log.d(TAG, "onCreate: ${Firebase.auth.currentUser?.uid}")
        if (mAuth.currentUser!=null){
            moveToMenuActivity(mAuth.currentUser!!.uid)
        }

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)


        val btLogin = binding.buttonLogin
        btLogin.setOnClickListener {
            try {
                val username = binding.etUsename.text.toString()
                if (TextUtils.isEmpty(username)) throw Exception("Email tidak boleh kosong")
                val password = binding.etPassword.text.toString()
                if (TextUtils.isEmpty(password)) throw Exception("Password tidak boleh kosong")

                loginUserAccount(username, password)
            } catch (e: Exception) {
                Snackbar.make(
                    findViewById(R.id.activity_main),
                    "${e.localizedMessage}",
                    Snackbar.LENGTH_SHORT
                ).show()
            }
        }
    }

    private fun loginUserAccount(username: String, password: String) {
        binding.buttonLogin.visibility = View.GONE
        binding.progressBar.visibility = View.VISIBLE

        mAuth.signInWithEmailAndPassword(username, password)
            .addOnCompleteListener {

                if (it.isSuccessful) {
                    Toast.makeText(applicationContext, "Login successful", Toast.LENGTH_SHORT).show()
                    moveToMenuActivity(it.result.user!!.uid)
                } else {
                    Snackbar.make(
                        findViewById(R.id.activity_main),
                        "Login failed!!",
                        Snackbar.LENGTH_SHORT
                    ).show()
                    Log.e(TAG, "loginUserAccount: ${it.exception?.localizedMessage}")
                }

                binding.buttonLogin.visibility = View.VISIBLE
                binding.progressBar.visibility = View.GONE
            }
    }

    private fun moveToMenuActivity(uid: String) {
        // Enable Firestore logging
        FirebaseFirestore.setLoggingEnabled(true)

        // Firestore
        firestore = Firebase.firestore

        // Get the 10 newest presence
        query = firestore.collection(Staff.COLLECTION_NAME)
            .whereEqualTo(Staff.UID, uid)
            .limit(1)

        query!!.get().addOnCompleteListener { task ->
            if (task.isSuccessful){
                val result = task.result
                result?.let {
                    result.documents.mapNotNull { snapshot ->
                        Log.d(TAG, "snapshot = ${snapshot["staff_id"]}")
                        val staff = snapshot.toObject(Staff::class.java)
                        Log.d(TAG, staff.toString())
                    }
                }
            } else {
                task.exception?.let { Log.e(TAG, it.stackTraceToString()) }
            }
        }

        val intent = Intent(this, MenuActivity::class.java)
        startActivity(intent)
    }
}