package org.informatika.rajin.model

import android.content.Context
import android.text.TextUtils
import com.google.firebase.firestore.Query
import org.informatika.rajin.R

/**
 * Object for passing filters around.
 */
class Filters {

    var staffId: String? = null
    var from: String? = null
    var to = -1
    var sortBy: String? = null
    var sortDirection: Query.Direction = Query.Direction.DESCENDING


    companion object {

        val default: Filters
            get() {
                val filters = Filters()
                filters.sortBy = Presence.FIELD_TIME_CREATE
                filters.sortDirection = Query.Direction.DESCENDING

                return filters
            }
    }
}
