package org.informatika.rajin.model

import com.google.firebase.Timestamp
import com.google.firebase.firestore.IgnoreExtraProperties
import com.google.firebase.firestore.PropertyName

data class Presence(

    @get:PropertyName("app_log") @set:PropertyName("app_log") @PropertyName("app_log")
    var appLog: AppLog? = null,

    @get:PropertyName("staff_id") @set:PropertyName("staff_id") @PropertyName("staff_id")
    var staffId: String? = null,

    var jenis: String? = null,
    var ket: String? = null,

    @get:PropertyName("check_in") @set:PropertyName("check_in") @PropertyName("check_in")
    var checkIn: Cek? = null,

    @get:PropertyName("check_out") @set:PropertyName("check_out") @PropertyName("check_out")
    var checkOut: Cek? = null,

    @get:PropertyName("is_lembur") @set:PropertyName("is_lembur") @PropertyName("is_lembur")
    var isLembur: Boolean? = null,

    @get:PropertyName("time_create") @set:PropertyName("time_create") @PropertyName("time_create")
    var timeCreate: Timestamp? = null,

    @get:PropertyName("time_update") @set:PropertyName("time_update") @PropertyName("time_update")
    var timeUpdate: Timestamp? = null
) {

    override fun toString(): String {
        return "staffId : $staffId, jenis : $jenis, isLembur : $isLembur, checkInTime : ${checkIn?.waktu?.toDate()}, checkInLoc : GeoPoint(${checkIn?.location?.latitude}, ${checkIn?.location?.longitude})"
    }

    companion object {
        const val COLLECTION_NAME = "presence"
        const val FIELD_TIME_CREATE = "time_create"
    }
}