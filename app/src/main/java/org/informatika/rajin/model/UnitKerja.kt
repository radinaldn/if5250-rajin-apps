package org.informatika.rajin.model

import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.PropertyName
import org.informatika.rajin.model.Wilayah

data class UnitKerja (
        var level: String? = null,
        var nama: String? = null,
        var parent: DocumentReference? = null,

        @get:PropertyName("batas_wilayah") @set:PropertyName("batas_wilayah") @PropertyName("batas_wilayah")
        var batasWilayah: List<Wilayah>? = null,
        ) {

        override fun toString(): String {
                return "level : $level, nama : $nama, batas_wilayah : $batasWilayah"
        }

        companion object {
                const val COLLECTION_NAME = "unit_kerja"
        }
}