package org.informatika.rajin.model

import com.google.firebase.firestore.GeoPoint
import com.google.firebase.firestore.PropertyName

data class Wilayah (
    var nama: String? = null,
    var polygons: List<GeoPoint>? = null,
        ) {

    override fun toString(): String {
        return "nama : $nama, polygons_length : ${polygons?.size}"
    }
}