package org.informatika.rajin.model

import com.google.firebase.firestore.PropertyName

data class AppLog(
    var type: String? = null,
    var version: String? = null
) {
    override fun toString(): String {
        return "type : $type, version : $version"
    }
}